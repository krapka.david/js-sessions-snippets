Basic function
````Javascript
// Function definition
var hello = function() {
   console.log('Hello world');
}

// Function call
hello();
/* 
Hello world
*/
````

Function parameters and arguments
````Javascript
// number1 and number2 are function parameters
var calcAdd = function (number1, number2) {
   console.log(number1 + number2);
}

// 5 and 10 are arguments for function call
calcAdd(5, 10);
/* 
15
*/
````

Return statement
````Javascript
var calcAdd = function (number1, number2) {
   var addNum = number1 + number2;
   return addNum;
}

// result is returned to the console log
console.log(calcAdd(5, 10));
/* 
15
*/

// result is returned to the variable
var result = calcAdd(5, 10);
````

CH1: Hello!
--
* create a **function** that will dynamically insert **name** to a **predefined string** (like "Hello. I am 'name'")
* that function will have **one parameter** that will accept value of a **name**
* function needs to **return the result** no console log inside the function
* **call the function** with chosen **name as a argument** inside **console log** afterwards

Solution
````Javascript
var hello = function (name) {
   return 'Hello. I am ' + name + '.';
}

console.log(hello('David'));
````

Function expression and declaration
````Javascript
// Function expression
var calcAdd = function (number1, number2) {
   var addNum = number1 + number2;
   return addNum;
}

// Function declaration
function calcAdd(number1, number2) {
   var addNum = number1 + number2;
   return addNum;
}
````

Function scope
````Javascript
// CASE 1
var car = 'Fiat'
var test = function () {
   console.log(car); // undefined
   var car = 'BMW'
   console.log(car); // BMW
}

test();

console.log(car); // Fiat


// CASE 2
var car = 'Fiat'
var test = function () {
   console.log(car); // Fiat
}

test();

console.log(car); // Fiat


// CASE 3
var test = function () {
   var car = 'BMW';
}

test();

console.log(car); // Not defined - error
````

CH2: Find a country
--
* copy this array:
````Javascript
var countries = ['Brazil', 'Austria', 'France', 'Germany'];
````
* create a function **findCountry** that will accept one argument - **country** and tries to find it within the countries array
* **return** string responds (like 'Found it' or 'Not found') for cases when the searched **string is in the array or not**
* **log the function call** (try both cases)

Solution
````Javascript
var countries = ['Brazil', 'Austria', 'France', 'Germany'];

var findCountry = function (country) {
   for (var i = 0; i < countries.length; i++) {
      if (countries[i] === country) {
         return 'Found it'
      } else {
         return 'Not found'
      }
   }
}

console.log(findCountry('Austria'));
````