FUNCTION SCOPE QUESTIONS
* **undefined** - variable exists but does not have a value
* **not defined** - variable does not exist

````Javascript
var whichDay = function() {
   day = 'Thursday';
   return day;
}

var result = whichDay();
console.log(result);
````
result = 'Thursday' valid, JS will create global variable

````Javascript
var day = 'Monday';
var whichDay = function() {
   var day = 'Thursday';
   return day;
}

var result = whichDay();
console.log(result);
console.log(day);
````
result = 'Thursday' and 'Monday'

````Javascript
var whichDay = function () {
   var day = 'Thursday';
   return day;
}
console.log(day);
````
result = not defined

````Javascript
var day;
var whichDay = function () {
   var day = 'Thursday';
   return day;
}
console.log(day);
````
result = undefined

TASK - HOMEWORK
* Copy this array
````Javascript
var elements = ['DIV', 'IMG', 'SPAN', 'SECTION', 'MAIN', 'SPAN'];
````
* Create function that will loop through following array and tell you whether it founds element passed to an argument. If it founds nothing, tell user that it founds nothing too.
* **"Be gentle!"**: you can use console.log inside a function
* **"Bring it on!"**: using console.log inside a function is forbidden
* **"I own Doom!"**: log also the name of the element
* **"Watch me die!"**: log also how many elements you found (try it on the SPAN one)

Solution
````Javascript
var elements = ['DIV', 'IMG', 'SPAN', 'SECTION', 'MAIN', 'SPAN'];

var findElement = function (element) {
   var result;
   var count = 0;

   elements.forEach(function (item) {
      if (item === element) {
         count++;
         result = 'Found ' + count + ' ' + item;
         
      }
   });

   if (!result) {
      result = 'Not found!';
   }

   return result;
}

var result = findElement('SPAN');
console.log(result);
````

BASIC OBJECT
````Javascript
var person = {
   firstName: 'Chuck',
   lastName: 'Norris'
}

console.log('This is ' + person.firstName + ' ' + person.lastName)
// This is Chuck Norris
````

METHODS AND THIS
````Javascript
var teenCalculator = {
   prependText: 'OMG, the result is: ',
   appendText: ' .That\'s lame, I wanna go home.',
   add: function(num1, num2) {
      return this.prependText + (num1 + num2) + this.appendText;
   },
   multiply: function (num1, num2) {
      return this.prependText + (num1 * num2) + this.appendText;
   }
}

var result1 = teenCalculator.add(5, 45);
console.log(result1);
// OMG, the result is: 50. That's lame, I wanna go home.

var result2 = teenCalculator.multiply(10, 8);
console.log(result2);
// OMG, the result is: 80. That's lame, I wanna go home.
````

CH2: Temperature converter 2.0
--
* create **object** that will have **two methods**
* one will be for converting **Fahrenheit temperature to Celsius**
* second will be for converting **Celsius temperature to Fahrenheit**
* both of them should **return result** - no console.log inside a method
* convert **50 Fahrenheit to Celsius**
* convert **25 Celsius to Fahrenheit**
* log results

Solution
````Javascript
var tempConverter = {
   toCelsius: function (fahrenheit) {
      return (fahrenheit - 32) / 1.8;
   },
   toFahrenheit: function (celsius) {
      return celsius * 1.8 + 32;
   }
}

var resultC = tempConverter.toCelsius(50);
console.log(resultC);

var resultF = tempConverter.toFahrenheit(25);
console.log(resultF);
````

HOMEWORK
````Javascript
var elements = ['DIV', 'IMG', 'SPAN', 'SECTION', 'MAIN', 'SPAN'];

var findElement = function (element) {
   var result;
   var count = 0;

   elements.forEach(function (item) {
      if (item === element) {
         count++;
         result = 'Found ' + count + ' ' + item;
         
      }
   });

   if (!result) {
      result = 'Not found!';
   }

   return result;
}

var result = findElement('SPAN');
console.log(result);
````