TRUTHY FALSY
````Javascript
if (val) {
   console.log("Truthy!");
} else {
   console.log("Falsy.");
}
````


BASIC ARRAY
````Javascript
var vegetables = ['garlic', 'tomato', 'potatoes'];
var groceries = ['coffee', 'bread', vegetables];

console.log(groceries[0]);
// coffee
console.log(groceries[2]);
// [ 'garlic', 'tomato', 'potatoes' ]
console.log(groceries[2][1]);
// tomato
````


CH1: Things to do
---
* create an **array** named **todo**
* insert there **3 things you should do** today
* log a message: **Don't forget to: [SECOND THING in your list]** - In example: *Don't forget to: make a coffee*


ADD TO THE END
````Javascript
var cars = ['BMW', 'Audi', 'Ferrari'];

cars.push('Bugatti');
console.log(cars);
// ['BMW', 'Audi', 'Ferrari', 'Bugatti']

// OR
var chevrolet = ['Camaro', 'Corvette'];
cars.push('Porsche', chevrolet)
console.log(cars);
// ['BMW', 'Audi', 'Ferrari', 'Bugatti', 'Porsche', chevrolet]
````


REMOVE FROM THE END
````Javascript
var countries = ['Germany', 'Slovakia', 'Poland'];

countries.pop();
console.log(countries);
// ['Germany', 'Slovakia']
````


ADD TO THE START
````Javascript
var years = [1984, 1995, 2008];

years.unshift(1955);
console.log(years);
// [ 1955, 1984, 1995, 2008 ]
````

REMOVE TO THE START
````Javascript
var books = ['Brave New World', 'Animal Farm', 'Atlas Shrugged']

books.shift();
console.log(books);
// [ 'Animal Farm', 'Atlas Shrugged' ]
````

CH2: Supermarket run
---
* you are in supermarket and you need to buy some things
* **cart** is your array and depending on which **section** of supermarket you are, you will need to put things to your cart
* you are aware about **vegetables**, **snacks** and **meat** sections
* you'll need **tomato** and **cucumber** from *vegetables* sec.
* you'll need **cookies** from *snacks* sec.
* you'll need **chicken wings** from *meat* sec.
* set **condition for every section visit**
<br><br>
**SCENARIO 1:** 
* you arrived to the **vegetables** section
* **grab needed items** and **log the content** of your cart
<br><br>
**SCENARIO 2:**
* you arrived to the **snacks** section
* **grab needed item**
* you decided that the cookies are all you need so you will go straight to the checkout
* after few seconds you realize that you forget your money so you will need to **put it back**
* **log the content** of your cart


ADD OR REMOVE ONE OR MORE ITEMS FROM ANY PLACE
````Javascript
var sports = ['football', 'tennis', 'golf', 'ice hockey', 'baseball', 'volleyball'];

sports.splice(1, 2);
console.log(sports);
// [ 'football', 'ice hockey', 'baseball', 'volleyball' ]

sports.splice(2, 0, 'basketball');
console.log(sports);
// [ 'football', 'ice hockey', 'basketball' 'baseball', 'volleyball' ]
````


FIND OUT THE ARRAY'S LENGTH
````Javascript
var religions = ['christianity', 'islam', 'budhism', 'hinduism'];

console.log(religions.length);
// 4
````


CH3: Around the world
---
* Lets make an array - **european cities** and insert there **five** of your favourite cities (european
* Make an empty array - **american cities**
* Under them make another empty array - **world cities**
<br><br>
**SCENARIO 1:**
* dynamically insert **european cities** array into the **world cities**
* **choose one** and **log it from world cities** array
<br><br>
**SCENARO 2:**
* dynamically fill **american cities** with **3 cities** from this continent, preferably in **one row** (with one method)
* dynamically insert **american cities** array into the **world cities**
* dynamically remove **european cities** from **world cities**
* **log last item of european cities** from **world cities** array with a use of **length property**