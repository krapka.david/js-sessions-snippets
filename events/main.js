const filters = document
   .querySelector('select[name="filter-type"]')
   .querySelectorAll('option')

const events = document.querySelectorAll('.events__event')

const activeFilters = []

filters.forEach(function(filterItem) {
   filterItem.addEventListener('click', function(item) {
      const currentFilter = item.target.value
      const filterPosition = activeFilters.indexOf(currentFilter)

      // PUSH OR REMOVE FILTER VALUES
      if (filterPosition !== -1) {
         activeFilters.splice(filterPosition, 1)
      } else {
         activeFilters.push(currentFilter)
      }

      // SHOW OR HIDE EVENTS ACCORDING TO THE USED FILTERS ARRAY
      events.forEach(function (event) {
         const eventType = event.dataset.type

         if (activeFilters.indexOf(eventType) === -1) {
            event.classList.add('js-hidden')
         } else {
            event.classList.remove('js-hidden')
         }
      })

      // ALL TYPES BUTTON WILL REVEAL ALL EVENTS
      if (currentFilter === 'all') {
         events.forEach(function(event) {
            event.classList.remove('js-hidden')
         })
      }

      console.log(activeFilters)
      
   })
})

