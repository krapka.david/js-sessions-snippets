Updating value
````Javascript
var num = 3;

// More descriptive
num = num + 3;
// Shortcut
num += 3;
````

Concat
````Javascript
var logInStatic = 'Hello Robert, You are logged in.';

var user = 'Robert';
var logInDynamic = 'Hello ' + user + '. You are logged in.';
````