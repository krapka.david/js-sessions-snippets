SELECT DOM ELEMENT (NODE)
````Javascript
// Values inside querySelector behaves like CSS selectors
var bodyEl = document.querySelector('body')
console.log(bodyEl) // Return <body> element with its predefined properties and methods
````

MANIPULATING WITH DOM
````Javascript
var bodyEl = document.querySelector('body')
bodyEl.style.color = 'tomato' // Add inline style
````

ADDING A CLASS
````Javascript
var bodyEl = document.querySelector('body')
bodyEl.classList.add('body') // Add css class - much better

bodyEl.classList.remove('body') // And you can easily remove it (or any other)
````

TRAVERSING DOM
````Javascript
var bodyEl = document.querySelector('body')

// List all child nodes of body element
bodyEl.childNodes.forEach(function (node) {
   // If the node item has a name of DIV, log him
   if (node.nodeName === 'DIV') {
      console.log(node)
   }
})
````
````Javascript
var divEl = document.querySelector('div')

console.log(divEl.parentNode)
````

SELECTING MULTIPLE ELEMENTS
````Javascript
var divs = document.querySelectorAll('div')
// Change background color for every div
divs.forEach(function (div) {
   div.style.backgroundColor = 'skyblue'
})
````

CREATE NEW ELEMENT AND APPEND IT INSIDE EXISTING ONE
````Javascript
// What? - H1
var newHeading = document.createElement('H1')
// Text of the H1
newHeading.textContent = 'Bazinga'
// Where? - inside BODY
var bodyEl = document.querySelector('body')
bodyEl.appendChild(newHeading)
````

DO SOMETHING WHEN YOU CLICK SOMEWHERE
````Javascript
// Select button
var buttonEl = document.querySelector('button')
// Listen for click on that button
buttonEl.addEventListener('click', function () {
   var bodyEl = document.querySelector('body')
   
   // After click, create new paragraph with predefined text content and append to the end of body
   var newParagraph = document.createElement('p')
   newParagraph.textContent = 'Thank you!'
   bodyEl.appendChild(newParagraph)
})
````

Homework
* Grab ActionBro card and put it on end of the cards row
* Put new paragraph with some lorem ipsum text (or cat ipsum) under the cat cards
* CHALLENGING: Add 2 new catbros
* CHALLENGING: Try to do first task without using id's