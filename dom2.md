CONST & LET
````Javascript
const car = 'BMW' // Immutable, you cannot change a value
car = 'Audi' // Output error 

let mobileOS = 'Android' // Block scoped, mutable
mobileOS = 'iOS' // Without errors
````

STRICT MODE
````Javascript
'use strict'

car = 'BMW' // Error - not declared

// Better error handling
````

DO SOMETHING WHEN YOU CLICK SOMEWHERE
````Javascript
// Select button
const buttonEl = document.querySelector('button')
// Listen for click on that button
buttonEl.addEventListener('click', function () {
   const bodyEl = document.querySelector('body')

   // After click, create new paragraph with predefined text content and append to the end of body
   const newParagraph = document.createElement('p')
   newParagraph.textContent = 'Thank you!'
   bodyEl.appendChild(newParagraph)
})
````