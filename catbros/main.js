'use strict'

const cards = document.querySelectorAll('.cards__card')
const main = document.querySelector('main')

cards.forEach(function(card) {
   card.addEventListener('click', function(item) {
      const newParag = document.createElement('p')
      newParag.textContent = item.target.src
      main.appendChild(newParag)
   })
})