Basic for loop
````Javascript
var loopsCount = 5;
for (var i = 0; i < loopsCount; i++) {
   console.log(i);
}
/*
0
1
2
3
4
*/
````

Real world loop
````Javascript
var array = ['tomato', 'apple', 'oranges'];
for (var i = 0; i < array.length; i++) {
   console.log(array[i]);
}
/*
tomato
apple
oranges
*/
````

CH1: Bazinga!
---
* create an **array** and fill it **five different strings**
* use for loop to **change all the items to bazinga**
* log the array in the end

![](https://media.giphy.com/media/CV61LRKyQf6P6/giphy.gif)

Solution
---
````Javascript
var array = ['Sitemuse', 'RedTiger', 'Azure', 'Outlook', 'MASH'];
for (let i = 0; i < array.length; i++) {
   array[i] = 'Bazinga!'
}

console.log(array);
````

Break
````Javascript
var array = ['Brazil', 'Chile', 'Colombia', 'Cuba', 'Ecuador'];
for (var i = 0; i < array.length; i++) {
   if (array[i] === 'Chile') {
      console.log('Found it!');
      break; // Looping will end with 'Chile'
   }
}
````

Continue
````Javascript
var array = ['Brazil', 'Chile', 'Bazinga!', 'Colombia', 'Cuba', 'Ecuador', 'Bazinga!'];
for (var i = 0; i < array.length; i++) {
   if (array[i] === 'Bazinga!') {
      console.log('Found it!');
      continue; // Output will exclude 'Bazinga!'
   }
}
````

CH2: Headings not invited
---
* create following array:
````Javascript
var elements = ['DIV', 'IMG', 'H2', 'SPAN', 'FORM', 'H3'];
````
* create an **empty array** named **vipElements**
* loop through elements array and **push to the vipElements** only that **items that are not headings**
* log final vipElements array

Solution
---
````Javascript
var elements = ['DIV', 'IMG', 'H2', 'SPAN', 'FORM', 'H3'];
var vipElements = [];

for (var i = 0; i < elements.length; i++) {
   if (elements[i] === 'H2' || elements[i] === 'H3') {
      continue;
   } else {
      vipElements.push(elements[i]);
   }
}

console.log(vipElements);
````


while loop
````Javascript
var i = 0;
while (i < 3) {
   console.log('Loop number ' + i);
   i++;
}
/*
Loop number 0
Loop number 1
Loop number 2
*/
````


CH3: Choo choo
---
* there is a **train** that has a **length of 7 vagons**
* create a **while loop** that will loop **through all the vagons**
* first **two vagons** are **1st class**
* **all other but the last** are **2nd class**
* **last one** is a **post vagon**
* **output all the vagons to the console**

Solution
---
````Javascript
var trainLength = 0;
while (trainLength < 7) {
   if (trainLength < 2) {
      console.log('1st class vagon')
   } else if (trainLength >= 2 && trainLength < 6) {
      console.log('2nd class vagon')
   } else {
      console.log('post vagon')
   }
   trainLength++;
}
````


forEach
````Javascript
var filterCategories = ['country', 'month', 'type', 'level'];
filterCategories.forEach(function(item) {
   console.log(item);
});
/* 
country
month
type
level
*/
````