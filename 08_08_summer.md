Everyday playlist
---
````Javascript
var playlists = [chillout, energizingMusic, classicalMusic, electronicMusic, podcasts];
var situation = '';

if (situation === 'work') {
   playlists = [];
   playlists.push(chillout, electornicMusic);
} else if (situation === 'relaxing') {
   playlists = [];
   playlists.push(chillout, classicalMusic);
} else if (situation === 'travelling') {
   playlists = [];
   playlists.push(podcasts);
} else if (situation === 'exercising') {
   playlists = [];
   playlists.push(energizingMusic);
} else {
   console.log('Please choose your mood.');
}
````

Make a tip
---
````Javascript
var highTip = 0.2;
var mediumTip = 0.1;
var lowTip = 0.05;

var payment = 150;
var tip = lowTip;

if (tip === 0.2 || tip === 0.1 || tip === 0.05) {
   var final = payment + (payment * tip);
   console.log(final);
} else {
   console.log('Please choose a tip.');
}
````

Temperature converter
---
````Javascript
var celsius = 30;
var fahrenheit;

fahrenheit = (celsius * 1.8) + 32;

console.log(fahrenheit);
````