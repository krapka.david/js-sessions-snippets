Basic for loop
````Javascript
var loopsCount = 5;
for (var i = 0; i < loopsCount; i++) {
   console.log(i);
}
/* 
0
1
2
3
4
*/
````

Loop through an array
````Javascript
var array = ['tomato', 'apple', 'oranges'];
for (var i = 0; i < array.length; i++) {
   console.log(array[i]);
}
/* 
tomato
apple
oranges
*/
````

<br>

CH3: Bazinga!
---
* create an **array** and fill it **five different strings**
* use for loop to change **all the items to bazinga**
* log the array in the end

<br>

Break statement
````Javascript
var array = ['Brazil', 'Chile', 'Colombia', 'Cuba', 'Ecuador'];
for (var i = 0; i < array.length; i++) {
   if (array[i] === 'Chile') {
      console.log('Found it!');
      break; // Looping will end with Chile
   }
}
````

Continue statement
````Javascript
var array = ['Brazil', 'Chile', 'Bazinga!', 'Cuba', 'Ecuador', 'Bazinga!'];
for (var i = 0; i < array.length; i++) {
   if (array[i] === 'Bazinga!') {
      continue; // Output will exclude Bazinga!
   }
   console.log(array[i]);
}
/* 
Brazil
Chile
Cuba
Ecuador
*/
````

<br>

CH4: Headings not invited
---
* create following array:
````Javascript
var elements = ['SPAN', 'DIV', 'H1', 'IMG', 'H2'];
````
* create an **empty array** named **vipElements**
* loop through elements array and **push to the vipElements** only that **items that are not headings**
* log final vipElements array

<br>

While loop
````Javascript
var i = 0;
while (i < 5) {
   i++
   console.log(i);
}
/* 
1
2
3
4
5
*/
````

Do while loop
````Javascript
var i = 0;
do {
   i++
   console.log(i);
} while (i < 5);
/* 
1
2
3
4
5
*/
````

forEach method
````Javascript
var array = ['Audi', 'BMW', 'Mercedes'];
array.forEach(function (element) {
   console.log(element);
});
````