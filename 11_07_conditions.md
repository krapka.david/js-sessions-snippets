IF
````javascript
var isLoggedIn = true;
if (isLoggedIn === true) {
   console.log('Welcome user!');
}

// or shortened
if (isLoggedIn) {
   console.log('Welcome user!');
}
````


COMPARISON OPERATORS
````javascript
// Equal
(8 === 8)
('microsoft' === 'microsoft')

// Not equal
(8 !== 4)
('Microsoft' !== 'microsoft')

// Greater than
(8 > 4)

// Less than
(4 < 8)

// Negative check - NOT
(!isFilled)
````


CH1: Am I a Batman?
---
* declare **is batman** variable and set it to **true** (boolean)
* use the if statement to check whether you are a batman - if yes than log **"I am Batman"**
* log it
* try to change variable value to false - it shouldn't log anything


ELSE
````javascript
var isLoggedIn = true;
if (isLoggedIn) {
   console.log('Welcome user!');
} else {
   console.log('Welcome! Please log in.');
} 
````


CH2: Am I a Batman? 2.0
---
* set **is batman** variable to false
* create another condition for that false statement that will log **"I am not batman enough"**
* log it


ELSE IF
````javascript
var isAdmin = true
var isLoggedIn = true;
var user = 'Chuck Norris';
if (isAdmin) {
   console.log('Welcome admin!');
} else if (isLoggedIn) {
   console.log('Welcome ' + user + '!')
} else {
   console.log('Welcome! Please log in.');
}
````


CH3: Am I a Batman? 3.0
---
* delete **isBatman** variable
* create **hero** variable and set it to **"Robin"**
* change conditions in way so it will check for string **"Batman"** resulted in **"I am Batman"**
* it will also check for string **"Robin"** - result will be logged as **"So now I am Robin :("**
* last check will accept every other value and result to **"I am not batman enough"**
* log it
* you can play with with different string values for hero variable to see different results


NESTED IF
````javascript
var haveXbox = true;
var isJarda = true;

if (haveXbox) {
   if (isJarda) {
      console.log("Forza is the best game in the World!");
   }
}

// OR
if (haveXbox && isJarda) {
   console.log("Forza is the best game in the World!");
}
````


LOGICAL OPERATORS
````javascript
// Logical AND - everything must pass
(thisNeedsToBeTrue && trueVariable === true && anotherVariable !== false)

// Logical OR - something must pass
(thisCouldBeTrue || maybeVariable === 'maybe' || mathResult === 12)
````


CH4: 18+
---
* You are creating access check for adult content site
* User can pass only when his age **is 18 or more** and he **is not from restricted country**
* if his age is **under 18** - log that he is too young
* if his country is a **restricted one** - inform him about that through log
* if he **passes** both conditions - log that he has an access


CH5: MAKE A TIP
---
* create a **payment** variable with some price value
* create **tip** variable with either **'high'**, **'medium'** or **'low'** string value
* high tip is a **20%**, medium is **10%** and low is **5%**
* check how high is the tip and let it **compute** and **log** the final value
* bonus: include a case where the user **haven't specified a tip**